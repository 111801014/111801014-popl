(*Question 1*)

type var = string;
datatype expr = variable of var
       | e of expr*expr
       | func of var*expr
       
(*Question 3*)

fun del ([] : var list,item : var) = []
  | del (x::xs : var list,item : var) = if item = x then del(xs,item) else x::del(xs,item)  

fun free (variable(v)) = [v]
  | free (e(e1,e2)) = free(e1) @ free(e2)
  | free (func(v,exp)) = del(free(exp),v)
