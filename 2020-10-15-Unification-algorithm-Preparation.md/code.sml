datatype lam = V of string
        | A of lam * lam
        | L of string * lam
        
datatype hlam = HV of string
        | HL of hlam -> hlam
        | HA of hlam * hlam
        
(*Question 1*)

fun subset (x,u) (HV e) = if e = x then u else HV e
    |subset (x,u) (HA(e1,e2)) = HA (subset (x,u) e1, subset (x,u) e2)
    |subset (x,u) (HL f) = 
    let
       fun fp inp = subset (x,u) (f inp)
    in 
        HL fp
    end
     
(*Question 2*)

fun abstract x mh =
    let
        fun f nh = subset (x,nh) mh
    in
        HL f
    end
     