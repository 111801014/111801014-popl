signature SIG =
sig
    type symbol
    val arity: symbol -> int
    structure Ord: ORD_KEY where type ord_key = symbol
end

signature VAR = sig
    type var
    structure Ord: ORD_KEY where type ord_key = var
end

functor Term (S : SIG) (V: VAR) =
struct
    datatype term = VarTerm of V.var | SigTerm of (S.symbol * term list)

    fun occurs (VarTerm tv, v: V.var) = if V.Ord.compare (tv, v) = EQUAL then true else false
        | occurs (SigTerm (_, xs), v: V.var) = 
            let
                fun occursUtil [] = false
                    | occursUtil (x::xs) = if occurs (x, v) then true else occursUtil(xs)
            in
                occursUtil (xs)
            end

    (* if arity is not equal to the number of args of function symbol then return false *)
    fun isValid (SigTerm (sym, tlist)) = 
            let
                fun isValidUtil [] = true
                    | isValidUtil (x::xs) = (isValid x) andalso (isValidUtil xs)
            in
                (S.arity sym = List.length(tlist)) andalso (isValidUtil tlist)
            end
        | isValid (VarTerm x) = true

end

functor Telescope (S : SIG) (V: VAR)  =
struct
    structure term = Term (S) (V)

(*////////// Users red black map for managing the telescope ///////   *)
    structure telescope = RedBlackMapFn(V.Ord)

    type map = term.term telescope.map

    val empty = telescope.empty

    fun isValidInsertTerm (tele: map) (x: V.var) (term.VarTerm v) = 
                    let
                        val search = telescope.find(tele, x)
                    in
                        case search of
                           SOME trm => if term.occurs(trm, v) then false else true
                         | NONE => true
                    end
        | isValidInsertTerm (tele: map) (x: V.var) (term.SigTerm (sym, ls)) =
            (S.arity sym = List.length(ls)) andalso (isValidInsertLs tele x ls)

    and isValidInsertLs (tele: map) (v: V.var) [] = true
        | isValidInsertLs (tele: map) (v: V.var) (x::xs) = 
                 if ((isValidInsertTerm tele v x) andalso (isValidInsertLs tele v xs)) then true else false

    (* insert a map into telescope, if it is valid by checking the above cond *)
    fun insert (tele: map) (x: V.var) (term.VarTerm t) : map option =  if V.Ord.compare (x, t) = EQUAL
                                                        then SOME tele
                                                        else let
                                                            val search = telescope.find(tele, x)
                                                        in
                                                            case search of
                                                               SOME trm =>  if term.occurs(trm, t) 
                                                                            then NONE
                                                                            else if (isValidInsertTerm tele x (term.VarTerm t))
                                                                            then SOME (telescope.insert(tele, x, term.VarTerm t))
                                                                            else NONE
                                                             | NONE =>  if (isValidInsertTerm tele x (term.VarTerm t))
                                                                        then SOME (telescope.insert(tele, x, term.VarTerm t))
                                                                        else NONE
                                                        end
        | insert (tele: map) (x: V.var) (term.SigTerm (sym, ls)) : map option = let
                                                            val t = term.SigTerm (sym, ls)
                                                        in
                                                            if term.occurs(t, x) 
                                                            then NONE
                                                            else if (isValidInsertLs tele x ls)
                                                            then SOME (telescope.insert(tele, x, t))
                                                            else NONE
                                                        end
end

functor Unify (S : SIG) (V: VAR) =
struct
    structure telescopes = Telescope (S) (V)

    structure term = telescopes.term

    open telescopes

    open term

    fun lsToPairls [] _ = []
        | lsToPairls _ [] = []
        | lsToPairls (x::xs) (y::ys) = (x, y) :: (lsToPairls xs ys)

    fun unify (tele: map) (SigTerm (sym1, ls1), SigTerm (sym2, ls2)) : map option = if (S.arity sym1 = S.arity sym2) 
                                                                        andalso (S.arity sym1 = List.length (ls1)) 
                                                                        andalso (S.arity sym2 = List.length (ls2))
                                                                        then unifyList tele (lsToPairls ls1 ls2)
                                                                        else NONE
        | unify (tele: map) (VarTerm v1, VarTerm v2) : map option = let
                                                            val search = (telescope.find (tele, v1), telescope.find (tele, v2)) 
                                                        in
                                                            case search of
                                                               (NONE, NONE) => (insert tele v1 (VarTerm v2))
                                                               | (SOME x, NONE) => (insert tele v2 x)
                                                               | (NONE, SOME y) => (insert tele v1 y)
                                                               | (SOME x, SOME y) => (unify tele (x, y))
                                                        end
        | unify (tele: map) (VarTerm v, SigTerm (sym, ls)) : map option =    let
                                                                    val search = telescope.find (tele, v)
                                                                in
                                                                    case search of
                                                                       NONE => (insert tele v (SigTerm (sym, ls)))
                                                                     | SOME t => (unify tele (t, SigTerm (sym, ls)))
                                                                end
        | unify (tele: map) (SigTerm (sym, ls), VarTerm v) : map option = (unify tele (VarTerm v, SigTerm (sym, ls)))
    
    and unifyList (tele: map) ls : map option = 
        case ls of
        [] => SOME tele
        | x::xs => let 
                        val tele2 = (unify tele x) 
                    in
                        case tele2 of
                        NONE => NONE
                        | SOME t => (unifyList t xs) 
                    end


end

