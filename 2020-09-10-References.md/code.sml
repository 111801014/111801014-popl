(*Question 1*)
signature Counter = sig
    val get : unit -> int
    val increment : unit -> unit
    val decrement : unit->unit
end;

structure counter : Counter
 = struct
val count = ref 0;
fun get (a:unit) = !count;
fun increment (a:unit) = let val temp = count := !count + 1
		    in temp
		    end;
fun decrement (a:unit) = let val temp = if !count = 0 then count := !count
					 else count := !count - 1 
		    in temp
		    end;
 end;


(*Question 2*)
functor MKCounter() : Counter
= struct
val count = ref 0;
fun get (a:unit) = !count;
fun increment (a:unit) = let val temp = count := !count + 1
		    in temp
		    end;
fun decrement (a:unit) = let val temp = if !count = 0 then count := !count
					 else count := !count - 1 
		    in temp
		    end;
end;



structure A = MKCounter();
structure B = MKCounter();

