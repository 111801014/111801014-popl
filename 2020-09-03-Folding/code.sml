(* Solution 1 *)

(*fun foldr = ('summary -> 'elem -> 'summary) -> 'summary -> 'elem list -> 'summary*)

fun foldl (sfun)(s0) (x::xs) = foldl (sfun) (sfun(s0,x)) (xs)
 | foldl (sfun) (s0) ([]) = s0;

fun foldr (sfun) (s0) (x::xs) = sfun(x,(foldr (sfun) (s0) (xs)))
 | foldr (sfun) (s0) ([]) = s0;

(*Solution 2*)

val sfun = op +;
fun foldl (sfun) (s0) (x::xs) = foldl (sfun) (sfun(s0,x)) (xs)
 | foldl (sfun) (s0) ([]) = s0;

fun sum (x::xs) = foldl sfun 0 (x::xs)
| sum ([]) = 0;

(*Solution 3*)

(*Partition*)
fun sfun ((x::xs , y:: ys),boolfn(z)) = if boolfn(z)=x then (z::x::xs, y::ys)
					else 
					if boolfn(z)=y then (x::xs , z::y::ys)
					else
|sfun (([],[]),z) =if boolfn(z)=true then ([z], []) else ([] , [z]); 

(* initializing the left partition as true*)

fun partition boolFn (z::zs) = foldl sfun ([],[]) (z::zs)

(*Reversal*)
(* 'summary = 'a list *)
fun sfun (x::xs, y) = y:: (x::xs)
 | sfun ([], y) = [y];

fun reverse (y::ys) = foldl sfun [] (y::ys)
| reverse [] = [];

fun map f ls = let fun smap(z,y::ys) =  f z :: y :: ys
		     | smap (z,[]) = f z :: []    
		  in foldr smap [] ls
		  end;
fun partition f ls = let fun spar(z,(x,y)) = if f z then (z :: x,y)
					     else        (x,z :: y)
		     in foldr spar ([],[]) ls
	             end;
fun even n = if n mod 2 = 0 then true else false

datatype 'a option = NOT_PRESENT | SOME of'a;

fun nth (y, n) = let fun newf(m, (1,NOT_PRESENT)) = (1,SOME m)
	| newf (m, (p, NOT_PRESENT)) = (p-1, NOT_PRESENT)
	| newf (m, z) = z
	in #2 (foldl newf(n, NOT_PRESENT), y )
	end;
