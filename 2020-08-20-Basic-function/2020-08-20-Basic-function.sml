
(*1*)

(*
Type of uncurried function fn : ('a -> 'b -> 'c -> 'd) -> 'a* 'b * 'c -> 'd
Type of curried function fn : ('a* 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd
*)

fun curry f x y z = f (x,y,z);
fun uncurry f (x,y,z) = f x y z;

(*2*)

fun first (x,_) = x;
fun second(_,y) = y;

(*3*)

fun len nil = 0
	| len (x::xs) = 1 + (len xs);

(*4*)

fun rev nil = nil
  | rev (x::xs) = (rev xs) @ [x];
	
	
(*5*)

fun help 0 a b = a
  | help 1 a b = b
  | help n a b = help (n-1) b (a+b);
  
fun fib n = help n 0 1;

