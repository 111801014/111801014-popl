
(*Question 1*)
type var = string;
datatype expr = variable of var
       | func of var*expr
       | e of expr*expr;

datatype let_expr = variable_l of var
       | func_l of var*let_expr
       | e_l of let_expr*let_expr
       | LET of var*let_expr*let_expr;

datatype letrec_expr = variable_lr of var
       | e_lr of letrec_expr*letrec_expr
       | LETR of var*letrec_expr*letrec_expr
       | func_lr of var*letrec_expr
       | LETREC of var*letrec_expr*letrec_expr;

(*Question 2*)
fun Y (func_lr(v,exp)) = let val x = variable_lr "x" 
			 in  e_lr(func_lr("x",e_lr(func_lr(v,exp),e_lr(x,x))), func_lr("x",e_lr(func_lr(v,exp),e_lr(x,x))))
			 end			  
  
fun unletrec (variable_lr(x)) = variable_l(x)
  | unletrec (func_lr(v,exp)) = func_l(v,unletrec(exp))
  | unletrec (e_lr(e1,e2)) = e_l(unletrec(e1),unletrec(e2))
  | unletrec (LETR(v,e1,e2)) = LET(v,unletrec(e1),unletrec(e2))
  | unletrec (LETREC(v,e1,e2)) = LET(v,unletrec(Y (func_lr(v,e1))),unletrec(e2));    

fun unlet (variable_l(x)) = variable(x)
  | unlet (e_l(e1,e2)) = e(unlet(e1),unlet(e2))
  | unlet (func_l(v,exp)) = func(v,unlet(exp))
  | unlet (LET(v,e1,e2)) = e(func(v,unlet(e2)),unlet(e1));
 
