(*Assignment 1*)
fun map f [] = []
 | map f (x :: xs) = f x :: map f xs

(*Assignment 2*)
datatype 'a BT = NULL
 | Node of 'a BT*'a*'a BT (*Symbols- leftchild root rightchild*) 

(*Assignment 3*)
fun treemap f NULL = NULL 
  | treemap f (Node(l,d,r)) = Node(treemap f l,f d,treemap f r) 

(*Assignment 4*)
(*Type of all traversals 'a BinaryT -> 'a list*)					    
fun inorder NULL = []
  | inorder(Node(l,d,r)) = inorder l @ [d] @ inorder r
						
fun preorder NULL = []
  | preorder(Node(l,d,r)) = [d] @ preorder l @ preorder r

fun postorder NULL = []
  | postorder(Node(l,d,r)) = postorder l @ postorder r @ [d] 
					    
(*Assignment 5*)
(*Type inference 'a BT -> 'a BT*)
fun rightrotate (Node(Node(lsleft,lsdata,lsright),rootdata,rootright)) = Node(lsleft,lsdata,Node(lsright,rootdata,rootright))
